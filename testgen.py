import random
import io

nTestCase = 10                  # the number of test cases
nFileDigits = nTestCase//10+1

random.seed()                   # initialize the RNG
minA = 0                        # boundaries of the variable (eg. A = [0, 100])
maxA = 100
minB = 0
maxB = 100
minN = 1
maxN = 10

for i in range(1, nTestCase+1):
    # generate the input files
    fname = (str(i).zfill(nFileDigits)) + '.in'
    with io.open(fname,'w', newline='') as f:
        a = random.randrange(minA, maxA+1)
        b = random.randrange(minB, maxB+1)
        n = random.randrange(minN, maxN+1)
        f.write(str(a)+','+str(b)+','+str(n)+'\n')  # save to an input file
        f.close()

    # generate the output files
    fname = (str(i).zfill(nFileDigits)) + '.out'
    result = a**b % 10**n
    with io.open(fname,'w', newline='') as f:
        f.write(str(result)+'\n')
        f.close()
